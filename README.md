This is the tutorial we're working with at
https://gitlab.com/CxxProf/Documentation/wikis/Usage:-A-coding-tutorial

To use this you simply need to install via conan:

```
git clone <repo address>
mkdir build && cd build
conan install ..\cxxprof_tutorial\install
```